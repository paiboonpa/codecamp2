const Koa = require('koa');
const app = new Koa();
const render = require('koa-ejs');
const path = require('path');

render(app, {
    root: path.join(__dirname, 'views'),
    layout: false,
    viewExt: 'ejs',
    cache: false
});

app.use(async ctx => {
    await ctx.render('index');
});

app.listen(3000);